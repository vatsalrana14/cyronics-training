from flask import Flask, render_template

app = Flask(__name__)

# Using decorators to map the function with the web address.

@app.route("/")
def home():
    return 'hello this is main page <h1>HELLO </h1>'



# Using render_template to link a webpage.

@app.route("/mypage")
def mypage():

    return render_template("index.html")


 # Injecting values with the help of placeholders in html file
@app.route("/loginpage")
def loginpage():

    user = {'username':'Vatsal','status':'Login Successful!'}

    return render_template("loginpage.html", user=user)



 #iterating list values by inserting python code in html file.

@app.route("/emp_details")
def emp_details():

    emp = [
    {'name':'Vatsal Rana', 'city':'Silvassa,DNH'},
    {'name':'Shubham Pawar', 'city':'Mumbai,MH'},
    {'name':'Amit Mandal', 'city':'Pune,MH'}
    ]

    return render_template("emp_details.html", emp = emp)

# Running the app.

if __name__ == "__main__":
    app.run()
