from pymongo import MongoClient
from pprint import pprint
import pymongo
from datetime import datetime

#instace of MongoClient
client = MongoClient('localhost:27017')

#connecting to the database PPAP_OEE.
db = client['PPAP_OEE']

# collection variable
po_col = db['production_orders']

'''# iterating through the collection.
for x in po_col.find({}, {"_id":0}):
    pprint("_________________________________________________________________________________________")
    pprint(x)
'''
# fetching shift_records dated 24th september.
sr_col =  db['shift_records']

start = datetime(2020, 9, 24, 10, 0)
end = datetime(2020, 9, 25, 0, 0)

'''
for x in sr_col.find({}, {"_id":0}):
    if start <= x['SHIFT START TIME'] < end :
        pprint(x)
        pprint("_________________________________________________________________________________________")
'''
# fetching production_orders for 24 september
for x in po_col.find({'DATE':'24-sep-2020'}, {"_id":0}):
    pprint("_________________________________________________________________________________________")
    pprint(x)
count = po_col.count_documents({'DATE':'24-sep-2020'})
print('%d results found!'%(count))
